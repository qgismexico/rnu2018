<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+8" version="3.2.1-Bonn">
  <pipe>
    <rasterrenderer classificationMax="nan" classificationMin="nan" opacity="1" type="singlebandpseudocolor" alphaBand="-1" band="1">
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="EXACT" clip="0" classificationMode="1">
          <item label="0 - Unclassified" alpha="255" color="#000000" value="0"/>
          <item label="1 - Cuerpo de Agua" alpha="255" color="#0392ff" value="1"/>
          <item label="2 - Urbano" alpha="255" color="#afafaf" value="2"/>
          <item label="3 - Suelo Desnudo" alpha="255" color="#aa5500" value="3"/>
          <item label="4 - Agricultura" alpha="255" color="#ffff7f" value="4"/>
          <item label="5 - Nube" alpha="255" color="#ffffff" value="5"/>
          <item label="6 - Sombra" alpha="255" color="#000000" value="6"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation grayscaleMode="0" colorizeGreen="128" colorizeStrength="100" colorizeRed="255" colorizeOn="0" saturation="0" colorizeBlue="128"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
